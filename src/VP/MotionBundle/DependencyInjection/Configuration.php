<?php

namespace VP\MotionBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('vp_motion');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.


        $rootNode
            ->children()
                ->scalarNode('apiKey')
                    ->cannotBeEmpty()
                ->end()
            ->end()
            ->children()
                ->scalarNode('apiSecret')
                    ->cannotBeEmpty()
                ->end()
            ->end()
        ->end();


        return $treeBuilder;
    }
}
