<?php

namespace VP\MotionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VPMotionBundle:Default:index.html.twig');
    }
}
